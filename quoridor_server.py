#!/usr/bin/python
import os
import sys
import json
import random
import time

# os.mkfifo("input_pipe")

# pipeout = os.open("input_pipe", os.O_WRONLY)

# for i in range(100):
    # os.write(pipeout, str.encode(input()))

class Player:
    human = False
    input_fifo = ""
    output_fifo = ""

    def __init__(self, name):
        if name == "human":
            self.human = True
        else:
            input_fifo_name = "input_fifo_"+str(random.randint(0, 9999))
            output_fifo_name = "output_fifo_"+str(random.randint(0, 9999))
            if not os.path.isfile(input_fifo_name):
                os.mkfifo(input_fifo_name)
            if not os.path.isfile(input_fifo_name):
                os.mkfifo(output_fifo_name)
            command = f"cat {input_fifo_name} | {name} > {output_fifo_name} &"
            print(command)
            os.system(command)
            self.input_fifo = open(input_fifo_name, "w", 1)
            self.output_fifo = open(output_fifo_name, "r", 1)
            # self.output_fifo = os.open(output_fifo_name, os.O_RDONLY)
            # self.input_fifo = os.open(input_fifo_name, os.O_WRONLY)

    def send_status(self):
        # print("sending status")
        # if self.human:
            # print("human!")
        if not self.human:
            # print("bot!")
            self.input_fifo.write(json.dumps(status)+"\n")

    def receive_move(self):
        if self.human:
            move_type, x, y = input("move " + "OX"[current_player] + ": ").split()
            x, y = int(x), int(y)
            return (move_type, x, y)
        else:
            # print("reading bot move!")
            move_type, x, y = self.output_fifo.readline().split()
            x, y = int(x), int(y)
            return (move_type, x, y)

players = [Player(sys.argv[1]), Player(sys.argv[2])]

print(players)

intersec_used = [[False]*10 for i in range(10)]
status = {
        "v_walls" : [],
        "h_walls" : [],
        "current_player_pos" : (4, 0),
        "other_player_pos" : (4, 8),
        "current_player_wall_limit" : 10,
        "other_player_wall_limit" : 10,
        "current_player" : 0,
}

pos = [(4, 0), (4, 8)]

wall_limit = [10, 10]

dirs = [[1, 0], [0, 1], [-1, 0], [0, -1]]

current_player = 0

edges = [[[True for k in range(4)] for j in range(9)] for i in range(9)]

for x in range(9):
    edges[x][0][3] = False
    edges[x][8][1] = False

for y in range(9):
    edges[0][y][2] = False
    edges[8][y][0] = False

def print_board():
    print("   ", "   ".join(str(x) for x in range(9)))
    for y in range(8, -1, -1):
        out1 = (str(y) if y != 8 else " ") + " +"
        out2 = "  |"
        for x in range(9):
            out1 += ("   " if edges[x][y][1] else "---")
            out1 += "+"
            if (x, y) == pos[0]:
                out2 += " O "
            elif (x, y) == pos[1]:
                out2 += " X "
            else:
                out2 += "   "
            out2 += (" " if edges[x][y][0] else "|")
        print(out1)
        print(out2, y)
    print("  +" + "---+" * 9)
    print("     ", "   ".join(str(x) for x in range(8)))

def make_span(x, y, visited):
    for d in range(4):
        dx, dy = dirs[d]
        if edges[x][y][d] and (x+dx, y+dy) not in visited:
            visited.add((x+dx, y+dy))
            make_span(x+dx, y+dy, visited)

if __name__ == "__main__":
    turns = 0
    while True:
        print_board()
        while True:
            players[current_player].send_status()
            move_type, x, y = players[current_player].receive_move()
            px, py = pos[current_player]
            opx, opy = pos[1-current_player]

            cp_span, op_span = set(), set()

            if move_type == "m":
                legal_moves = []
                for d in range(4):
                    dx, dy = dirs[d]
                    if edges[px][py][d]:
                        if pos[1-current_player] != (px+dx, py+dy):
                            legal_moves.append((px+dx, py+dy))
                        elif edges[px+dx][py+dy][d]:
                            legal_moves.append((px+2*dx, py+2*dy))
                        else:
                            if edges[px+dx][py+dy][(d+1)%4]:
                                dx2, dy2 = dirs[(d+1)%4]
                                legal_moves.append((px+dx+dx2, py+dy+dy2))
                            if edges[px+dx][py+dy][(d-1)%4]:
                                dx2, dy2 = dirs[(d-1)%4]
                                legal_moves.append((px+dx+dx2, py+dy+dy2))
                if (x, y) not in legal_moves:
                    continue
                pos[current_player] = (x, y)
                status["current_player_pos"] = pos[1-current_player]
                status["other_player_pos"] = pos[current_player]
            elif move_type == "h":
                if wall_limit[current_player] == 0 or not (0 <= x < 8 and 0 <= y < 8):
                    continue
                if intersec_used[x][y] or (not edges[x][y][1]) or (not edges[x+1][y][1]):
                    continue
                intersec_used[x][y] = True
                edges[x][y][1] = False
                edges[x+1][y][1] = False
                edges[x][y+1][3] = False
                edges[x+1][y+1][3] = False
                make_span(px, py, cp_span)
                cp_ok = sum(1 for a, b in cp_span if b == 8*(1-current_player))
                make_span(opx, opy, op_span)
                op_ok = sum(1 for a, b in op_span if b == 8*current_player)
                if not cp_ok or not op_ok:
                    intersec_used[x][y] = False
                    edges[x][y][1] = True
                    edges[x+1][y][1] = True
                    edges[x][y+1][3] = True
                    edges[x+1][y+1][3] = True
                    continue
                status["h_walls"].append((x, y))
                wall_limit[current_player] -= 1;

            elif move_type == "v":
                if wall_limit[current_player] == 0 or not (0 <= x < 8 and 0 <= y < 8):
                    continue
                if intersec_used[x][y] or (not edges[x][y][0]) or (not edges[x][y+1][0]):
                    continue
                intersec_used[x][y] = True
                edges[x][y][0] = False
                edges[x][y+1][0] = False
                edges[x+1][y][2] = False
                edges[x+1][y+1][2] = False
                make_span(px, py, cp_span)
                cp_ok = sum(1 for a, b in cp_span if b == 8*(1-current_player))
                make_span(opx, opy, op_span)
                op_ok = sum(1 for a, b in op_span if b == 8*current_player)
                if not cp_ok or not op_ok:
                    intersec_used[x][y] = False
                    edges[x][y][0] = True
                    edges[x][y+1][0] = True
                    edges[x+1][y][2] = True
                    edges[x+1][y+1][2] = True
                    continue
                status["v_walls"].append((x, y))
                wall_limit[current_player] -= 1;

            break

        # if current_player has won, exit
        if pos[current_player][1] == 8 * (1 - current_player):
            break

        current_player = 1 - current_player
        status["current_player_pos"] = pos[current_player]
        status["other_player_pos"] = pos[1-current_player]
        status["current_player_wall_limit"] = wall_limit[current_player]
        status["other_player_wall_limit"] = wall_limit[1-current_player]
        status["current_player"] = current_player
        turns += 1

    print_board()
    print(f"game ended after {turns} turns")

    # clean all fifos
    os.system("rm *put_fifo_*")


